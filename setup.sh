#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Util methods
# -----------------------------------------------------------------------------
# Define a function to add a line to a file if it doesn't exist
append_line_to_file() {
    local line_to_add="$1"
    local target_file="$2"

    # Check if the line already exists in the target file
    if ! grep -qF "$line_to_add" "$target_file"; then
        echo "$line_to_add" >> "$target_file"
        echo "Line added to $target_file."
    else
        echo "Line already exists in $target_file. No changes made."
    fi
}

# This script is to be run on a Mac that will be used for an interactive.
sudo -v

if sudo -n true 2>/dev/null; then
    echo "Sudo credentials cached, continuing with privileged operations."
else
    echo "Sudo credentials not cached or incorrect password."
    echo "Exiting without performing privileged operations."
    exit 1
fi

# -----------------------------------------------------------------------------
# Homebrew
# -----------------------------------------------------------------------------
# if test ! $(which brew); then
#     echo "=> Installing Homebrew..."
#     ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# fi

echo "=> Installing Homebrew..."
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" -y

# Add to path
if [[ $(uname -m) == 'arm64' ]]; then
    echo "Apple Silicon"
    brew_dir=/opt/homebrew

    line_to_add='eval "$(/opt/homebrew/bin/brew shellenv)"'
    append_line_to_file "$line_to_add" ~/.zprofile
    append_line_to_file "$line_to_add" ~/.bash_profile

    eval "$(/opt/homebrew/bin/brew shellenv)"
else
    brew_dir=/usr/local

    line_to_add='eval "$(/usr/local/bin/brew shellenv)"'
    append_line_to_file "$line_to_add" ~/.zprofile
    append_line_to_file "$line_to_add" ~/.bash_profile

    eval "$(/usr/local/bin/brew shellenv)"
fi

brew install git
brew install wget
brew install curl
brew install python

# Add to path
if [[ $(uname -m) == 'arm64' ]]; then
    line_to_add="PATH=${brew_dir}/opt/python@3.11/libexec/bin:\$PATH"
    append_line_to_file "$line_to_add" ~/.zprofile
    append_line_to_file "$line_to_add" ~/.bash_profile
else
    line_to_add="PATH=${brew_dir}/opt/python/libexec/bin:\$PATH"
    append_line_to_file "$line_to_add" ~/.zprofile
    append_line_to_file "$line_to_add" ~/.bash_profile
fi

# -----------------------------------------------------------------------------
# Run some scripts
# -----------------------------------------------------------------------------
echo '=> Configure OS X..."'
chmod u+x osx-kiosk.sh && ./osx-kiosk.sh

echo

echo '=> Install Apps..."'
# chmod u+x install-apps.sh && ./install-apps.sh
chmod u+x install-brew-casks.sh && ./install-brew-casks.sh

echo '=> Install Xapp..."'
chmod u+x install-xapp.sh && ./install-xapp.sh

echo

source ~/.zprofile

echo 'DONE!'
