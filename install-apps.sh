#!/usr/bin/env bash

cpu_name="$(whoami)"
ip="$(ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}')"

# -----------------------------------------------------------------------------
# Helper methods
# -----------------------------------------------------------------------------
divline() {
    for i in {1..80};
        do printf "%s" "-";
    done;
    printf "\n"
}

file_exists() {
    if [ ! -f "$@" ]; then
        #echo "[x] File not found: $@"
        return 1
    else
        return 0
    fi
}

dir_exists() {
    if [ ! -d "$@" ]; then
        #echo "[x] Dir not found: $@"
        return 1
    else
        return 0
    fi
}


type_exists() {
    if [ $(type -P $1) ]; then
        return 0
    fi
    return 1
}

pause() {
    read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'
}

# Prompt to continue - Are you sure (y/n)?
confirm() {
    read -p "[?] $1? (y/n) "
    if [[ "$REPLY" =~ ^[Yy]$ ]]; then
        return 0
    else
        return 1
    fi
}

install_zip() {
    if ! dir_exists "/Applications/$1.app"; then
        divline
        if confirm "Do you want to install $1"; then
            echo "=> Install $1..."
            curl -L -o "$1.zip" "$2"
            unzip "$1.zip"
            mv "$1.app" /Applications
        fi
    else
        echo "$1 is already installed."
    fi
}

install_dmg() {
    if ! dir_exists "/Applications/$1.app"; then
        divline
        if confirm "Do you want to install $1"; then
            echo "=> Install $1..."
            curl -L -o "$1.dmg" "$2"
            hdiutil mount -nobrowse "$1.dmg" -mountpoint "/Volumes/$1"
            cp -R "/Volumes/$1/$1.app" /Applications
            hdiutil unmount "/Volumes/$1"
        fi
    else
        echo "$1 is already installed."
    fi
}

install_pkg() {
    if ! dir_exists "/Applications/$1.app"; then
        divline
        if confirm "Do you want to install $1"; then
            echo "=> Install $1..."
            curl -L -o "$1.dmg" "$2"
            hdiutil mount -nobrowse "$1.dmg" -mountpoint "/Volumes/$1"
            sudo installer -package "/Volumes/$1/Install $1.pkg" -target /
            hdiutil unmount "/Volumes/$1"
        fi
    else
        echo "$1 is already installed."
    fi
}

install_bzip() {
    if ! dir_exists "/Applications/$1.app"; then
        divline
        if confirm "Do you want to install $1"; then
            echo "=> Install $1..."
            curl -L -o "$1.tbz2" "$2"
            bzip2 -d "$1.tbz2" && tar xopf "$1.tar"
            mv "$1.app" /Applications
        fi
    else
        echo "$1 is already installed."
    fi
}

# -----------------------------------------------------------------------------
# Keep Alive
# -----------------------------------------------------------------------------
# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# -----------------------------------------------------------------------------
# Install Apps
# -----------------------------------------------------------------------------
# http://commandlinemac.blogspot.com/2008/12/installing-dmg-application-from-command.html

if ! dir_exists ~/Downloads/macapps_temp; then
    mkdir ~/Downloads/macapps_temp
fi
cd ~/Downloads/macapps_temp


install_zip "iTerm" "https://iterm2.com/downloads/stable/latest"
install_dmg "Google Chrome" "https://dl.google.com/chrome/mac/universal/stable/CHFA/googlechrome.dmg"
# install_dmg "Sublime Text" "https://download.sublimetext.com/Sublime%20Text%20Build%203143.dmg"
install_zip "Sublime Text" "https://download.sublimetext.com/sublime_text_build_4152_mac.zip"
install_pkg "TeamViewer" "http://download.teamviewer.com/download/version_15x/TeamViewer.dmg"

# -----------------------------------------------------------------------------
# Cleanup
# -----------------------------------------------------------------------------
echo "=> Cleaning up..."
rm -rfv ~/Downloads/macapps_temp
