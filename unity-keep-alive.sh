#!/usr/bin/env bash

# Keeps an app alive not matter what.  If it crashes or you quit it it will restart itself.
# The only way to stop it is to unload it via command line.

# To quit the app for any reason you must run:
#   launchctl unload ~/Library/LaunchAgents/user.launchkeep.app.plist

# To re-enable when ready you must run:
#   launchctl load ~/Library/LaunchAgents/user.launchkeep.app.plist

# -----------------------------------------------------------------------------

echo "Type Unity app name (eg - AppName), without the '.app' followed by [ENTER]:"

read app_name

# Create dirs we need
if [ ! -d "~/Library/LaunchAgents" ]; then
    mkdir -p ~/Library/LaunchAgents
fi

if [ ! -d "~/Applications" ]; then
    mkdir -p ~/Library/LaunchAgents
fi

# Create plist in LaunchAgents
cat > ~/Library/LaunchAgents/user.launchkeep.app.plist <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>Label</key>
  <string>user.launchkeep.app</string>
  <key>KeepAlive</key>
  <true/>
  <key>ProgramArguments</key>
  <array>
  	<string>/Applications/$app_name.app/Contents/MacOS/$app_name</string>
    <string>-screen-fullscreen</string>
    <string>1</string>
  </array>
</dict>
</plist>
EOF

launchctl load ~/Library/LaunchAgents/user.launchkeep.app.plist

# -----------------------------------------------------------------------------

echo "
To quit the app for any reason you must run:
    launchctl unload ~/Library/LaunchAgents/user.launchkeep.app.plist

To re-enable when ready you must run:
    launchctl load ~/Library/LaunchAgents/user.launchkeep.app.plist
"
