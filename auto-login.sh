#!/usr/bin/env bash

# -----------------------------------------------------------------------------

echo "Enabling automatic GUI login for the '$(whoami)' user.."
# Generate /etc/kcpassword
sudo rm /etc/kcpassword
sudo cp scripts/set_kcpassword.py /private/tmp/set_kcpassword.py
read -s -p "Enter Password: " pswd
sudo python /private/tmp/set_kcpassword.py $pswd
sudo defaults write /Library/Preferences/com.apple.loginwindow "autoLoginUser" `whoami`
sudo rm /private/tmp/set_kcpassword.py
