#!/usr/bin/env bash

# echo "=> Tap Caskroom..."
# brew tap "homebrew/cask"

# -----------------------------------------------------------------------------
# Install Apps
# -----------------------------------------------------------------------------
echo "=> Install apps..."
brew install google-chrome --cask --force
brew install iterm2 --cask --force
brew install sublime-text --cask --force
brew install teamviewer --cask --force
