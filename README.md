# OSX Kiosk
Author: Tim Santor <tsantor@xstudios.agency>

## Overview
This script automatically configures a Mac for "kiosk" mode. Saves time and keystrokes.

## Features
- General
    - Auto show/hide menu bar
- Desktop & Screensaver
    - Disable screen saver
- Dock
    - Enable minimize windows into application icon
    - Disable animate opening applications
    - Enable automatically hide and show the Dock
    - Show indicators for open applications
    - Remove all (default) app icons
- Mission Control
    - Remove hot corners
    - Disable displays have separate spaces
    - Disable Dashboard
- Language & Preferences
    - Set the timezone (EST)
    - Show the date on the clock
- Security & Privacy
    - Allow apps downloaded from Anywhere
    - Do not require passwords after sleep or screensaver
    - Enable automatic login
- Notification Center
    - Disable notification center
- Energy Saver
    - Never allow display or computer to sleep
    - Never allow hard disk(s) to sleep
    - Enable restart after power failure
    - Restart every day at 2am
    - Restart automatically if the computer freezes
- Mouse
    - Disable 'natural' scrolling
- Bluetooth
    - Turn off Bluetooth
- Sharing
    - Enable remote login (SSH)
- App Store
    - Disable automatically check for updates
    - Disable download apps purchased on other Macs
- Time Machine
    - Disable Time Machine
- Accessibility
    - Disable "shake mouse pointer to locate"
- Disable App Nap system wide
- Set some sane Finder preferences


## Software Installed (optional)
Installs the following commonly needed software when providing on-site and/or remote support:

- **iTerm** - a better terminal
- **Google Chrome** - a better browser
- **Sublime Text** - a better code editor (do not use Textedit)
- **TeamViewer 11** - for remote support (our corporate license also only covers version 11)


## Installation

    git clone https://bitbucket.org/xstudios/shell-osx-kiosk/ && cd shell-osx-kiosk

> No Git? Simply download the zip [here](https://bitbucket.org/xstudios/shell-osx-kiosk/downloads) and extract.


## Usage
CD into the new directory and run:

    ./setup.sh

Sit back and let the magic happen!

> **Note:** You will need to answer some prompts from time to time.
