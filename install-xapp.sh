#!/usr/bin/env bash

VENV_DIR=~/.venvs/xappmonitor_env

APP_NAME="xapp-monitor"
APP_SETTINGS_DIR=~/.${APP_NAME}
APP_CONFIG=${APP_SETTINGS_DIR}/${APP_NAME}.cfg
APP_LOG=${APP_SETTINGS_DIR}/${APP_NAME}.log

APP_PATH=${VENV_DIR}/bin/${APP_NAME}

echo "==> Install ${APP_NAME}"

# -----------------------------------------------------------------------------
# Helper methods
# -----------------------------------------------------------------------------

# Prompt to continue - Are you sure (y/n)?
confirm() {
    read -p "[?] $1? (y/n) "
    if [[ "$REPLY" =~ ^[Yy]$ ]]; then
        return 0
    else
        return 1
    fi
}

# -----------------------------------------------------------------------------
# Create Virtualenv
# -----------------------------------------------------------------------------

if [ ! -d ${VENV_DIR} ]; then
    [ -d ${VENV_DIR} ] || mkdir -p ${VENV_DIR}
    python3.11 -m venv ${VENV_DIR}
    source ${VENV_DIR}/bin/activate
    python3.11 -m pip install -U pip wheel
else
    echo "[✔] ${VENV_DIR}"
fi

# -----------------------------------------------------------------------------
# Install App
# -----------------------------------------------------------------------------

source ${VENV_DIR}/bin/activate

if ! which "${APP_PATH}" >/dev/null 2>&1; then
    python3.11 -m pip install https://xstudios-pypi.s3.amazonaws.com/xapp_monitor-latest-py2.py3-none-any.whl
else
    echo "[✔] $(command -v ${APP_PATH})"
fi

# -----------------------------------------------------------------------------
# Install & Configure Supervisor
# -----------------------------------------------------------------------------

if [[ $(uname -m) == 'arm64' ]]; then
    echo "Apple Silicon"
    brew_dir=/opt/homebrew
else
    brew_dir=/usr/local
fi

# eval "${brew_dir}/bin/brew shellenv)"

if [ ! -f ${brew_dir}/bin/supervisorctl ]; then
    echo "=> Install Supervisor..."
    brew install supervisor
    brew services restart supervisor
fi

# Create app ini
if [ ! -f ${brew_dir}/etc/supervisor.d/${APP_NAME}.ini ]; then
    mkdir -p ${brew_dir}/etc/supervisor.d/
    sh -c "cat > ${brew_dir}/etc/supervisor.d/${APP_NAME}.ini" <<EOF
[program:xapp_monitor]
command=${VENV_DIR}/bin/xapp-monitor --verbose
process_name=%(program_name)s
user=${USER}
autostart=true
autorestart=true
EOF
    echo "Created ${brew_dir}/etc/supervisor.d/${APP_NAME}.ini"
    supervisorctl reread
    supervisorctl update
    supervisorctl start xapp_monitor
fi

echo "Dont forget to ensure sudo requires no password."
echo "sudo nano /etc/sudoers"
echo "%admin  ALL=(ALL) NOPASSWD: ALL"
