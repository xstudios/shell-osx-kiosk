#!/usr/bin/env bash

# Author: Tim Santor <tsantor@xstudios.agency>

# This script is to be run on a Mac that will be used for an interactive.

# -----------------------------------------------------------------------------

# Might as well ask for password up-front, right?
sudo -v

# Keep-alive: update existing sudo time stamp if set, otherwise do nothing.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# -----------------------------------------------------------------------------
# Create an output log (eg - command >> ${LOG} 2>&1)
# -----------------------------------------------------------------------------
LOG=~/osx-kiosk.log
echo '' > ${LOG}

# -----------------------------------------------------------------------------
# General
# -----------------------------------------------------------------------------
set_general_perferences() {
    echo 'Setting "General" preferences...'

    # Auto hide/show the menu bar (finally!)
    defaults write NSGlobalDomain _HIHideMenuBar -bool true
}

# -----------------------------------------------------------------------------
# Desktop & Screen Saver
# -----------------------------------------------------------------------------
set_display_preferences() {
    echo 'Setting "Desktop & Screen Saver" preferences...'

    # Disable screensaver
    defaults -currentHost write com.apple.screensaver idleTime 0
}

# -----------------------------------------------------------------------------
# Dock
# -----------------------------------------------------------------------------
set_dock_preferences() {
    echo 'Setting "Dock" preferences...'

    # Enable minimize windows into application icon
    defaults write com.apple.dock minimize-to-application -bool true

    # Disable animate opening applications
    defaults write com.apple.dock launchanim -bool false

    # Enable automatically hide and show the Dock
    defaults write com.apple.dock autohide -bool true

    # Show indicators for open applications
    defaults write com.apple.dock show-process-indicators -bool true

    # Remove the auto-hiding Dock delay
    defaults write com.apple.dock autohide-delay -float 0

    # Wipe all (default) app icons from the Dock
    # This is only really useful when setting up a new Mac,
    # or if you don’t use the Dock to launch apps.
    defaults write com.apple.dock persistent-apps -array
}

# -----------------------------------------------------------------------------
# Mission Control
# -----------------------------------------------------------------------------
set_mission_control_preferences() {
    echo 'Setting "Mission Control" preferences...'

    # Set hot corners
    # Top left screen corner
    defaults write com.apple.dock wvous-tl-corner -int 0
    defaults write com.apple.dock wvous-tl-modifier -int 0

    # Top right screen corner
    defaults write com.apple.dock wvous-tr-corner -int 0
    defaults write com.apple.dock wvous-tr-modifier -int 0

    # Bottom left screen corner
    defaults write com.apple.dock wvous-bl-corner -int 0
    defaults write com.apple.dock wvous-bl-modifier -int 0

    # Bottom right screen corner
    defaults write com.apple.dock wvous-br-corner -int 0
    defaults write com.apple.dock wvous-br-modifier -int 0

    # Disable "Automatically rearrange Spaces based on most recent use"
    defaults write com.apple.dock mru-spaces -bool false

    # Disable "Group windows by application" (not working in El Crapitan?)
    defaults write com.apple.dock expose-group-by-app -bool false

    # Disable "Displays have separate Spaces"
    defaults write com.apple.spaces spans-displays -bool true

    # Don’t show Dashboard as a Space
    defaults write com.apple.dock dashboard-in-overlay -bool true

    # Disable Dashboard
    defaults write com.apple.dashboard mcx-disabled -bool true
}

# -----------------------------------------------------------------------------
# Language & Region
# -----------------------------------------------------------------------------
set_language_and_region_preferences() {
    echo 'Setting "Language & Region" preferences...'

    # Set the timezone
    # See `systemsetup -listtimezones` for other values"
    sudo systemsetup -settimezone "America/New_York" >> ${LOG} 2>&1
}

# -----------------------------------------------------------------------------
# Security & Privacy
# -----------------------------------------------------------------------------
set_security_preferences() {
    echo 'Setting "Security & Privacy" preferences...'

    # Disable gatekeeper (allows apps from anywhere to be installed)
    sudo spctl --master-disable

    # Do not require password when waking from sleep or screensaver
    # Defaults write does not work here, using apple script
    osascript -e 'tell application "System Events" to set require password to wake of security preferences to false'

    # Enable autoLogin for this user
    echo "Enabling automatic GUI login for the '$(whoami)' user.."
    sudo defaults write /Library/Preferences/com.apple.loginwindow autoLoginUser "$(whoami)"
    sudo defaults write /Library/Preferences/com.apple.loginwindow UseNameAndPassword -bool false

    # Enable autoLogin for this user
    # Generate /etc/kcpassword
    # sudo rm /etc/kcpassword
    # sudo cp scripts/set_kcpassword.py /private/tmp/set_kcpassword.py
    # read -s -p "Enter Password: " pswd
    # sudo python /private/tmp/set_kcpassword.py $pswd
    # sudo defaults write /Library/Preferences/com.apple.loginwindow "autoLoginUser" `whoami`
    # sudo rm /private/tmp/set_kcpassword.py
}

# -----------------------------------------------------------------------------
# Notifications
# -----------------------------------------------------------------------------
set_notification_preferences() {
    echo 'Setting "Notification" preferences...'

    # Set notification show previews to "Never" system-wide
    # defaults write com.apple.notificationcenterui DoNotDisturb -bool false
    # defaults write com.apple.notificationcenterui bannerTime 0

    # Disable notification center
    launchctl unload -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist >> ${LOG} 2>&1
    killall NotificationCenter >> ${LOG} 2>&1
}

# -----------------------------------------------------------------------------
# Energy Saver
# -----------------------------------------------------------------------------
set_energy_saver_preferences() {
    echo 'Setting "Energy Saver" preferences...'

    # Disable sleep
    sudo pmset -a sleep 0
    #sudo systemsetup -setcomputersleep off

    # Disable hard disk sleep
    sudo pmset -a disksleep 0
    #sudo systemsetup -setharddisksleep off

    # Disable display sleep
    sudo pmset -a displaysleep 0
    #sudo systemsetup -setdisplaysleep off

    # Disable power nap
    sudo pmset -c darkwakes 0

    # Disable power button sleep
    sudo pmset -c powerbutton 0
    #sudo systemsetup -setallowpowerbuttontosleepcomputer off

    # Enable restart after power failure
    sudo pmset -c autorestart 1
    #sudo systemsetup -setrestartpowerfailure on

    # Restart every day at 2am
    sudo pmset repeat cancel
    sudo pmset repeat restart MTWRFSU 02:00:00

    # Restart automatically if the computer freezes
    sudo systemsetup -setrestartfreeze on

    # Stop issue with display ports going to sleep?
    # https://discussions.apple.com/thread/6625408?start=0&tstart=0
    sudo pmset -a standbydelay 86400
    sudo pmset -a autopoweroffdelay 86400
    sudo pmset -a autopoweroff 0
    sudo pmset -a powernap 0
}

# -----------------------------------------------------------------------------
# Mouse
# -----------------------------------------------------------------------------
set_mouse_preferences() {
    echo 'Setting "Mouse" preferences...'

    # Turn off 'natural' scrolling
    defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false
}

# -----------------------------------------------------------------------------
# Bluetooth
# -----------------------------------------------------------------------------
set_bluetooth_preferences() {
    echo 'Setting "Bluetooth" preferences...'

    # Turn bluetooth off
    sudo defaults write /Library/Preferences/com.apple.Bluetooth.plist ControllerPowerState -bool false

    # Disable Assistant for Keyboard
    sudo defaults write /Library/Preferences/com.apple.Bluetooth.plist BluetoothAutoSeekKeyboard -bool false

    # Disable Assistant for Mouse/Trackpad
    sudo defaults write /Library/Preferences/com.apple.Bluetooth.plist BluetoothAutoSeekPointingDevice -bool false

    # sudo kextunload -b com.apple.iokit.BroadcomBluetoothHostControllerUSBTransport
    killall blued >> ${LOG} 2>&1
    launchctl unload /System/Library/LaunchDaemons/com.apple.blued.plist >> ${LOG} 2>&1
}

# -----------------------------------------------------------------------------
# Sharing
# -----------------------------------------------------------------------------
set_sharing_preferences() {
    echo 'Setting "Sharing" preferences...'

    # Enable Remote Login
    sudo systemsetup -setremotelogin on

    # sudo launchctl load -w /System/Library/LaunchDaemons/ssh.plist
}

# -----------------------------------------------------------------------------
# App Store
# -----------------------------------------------------------------------------
set_app_store_preferences() {
    echo 'Setting "App Store" preferences...'

    # TOGGLE ALL OFF (auto checking is on to show other prefs are toggled off)
    # before setting values quit system preferences & stop software update
    # - stops defaults cache breaking 'AutomaticCheckEnabled'
    osascript -e "tell application \"System Preferences\" to quit"

    # Disable system software updates
    sudo softwareupdate --schedule off >> ${LOG} 2>&1

    # Disable 'Automatically check for updates'
    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticCheckEnabled -bool false

    # Disable 'Download newly available updates in background'
    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticDownload -bool false

    # Disable 'Install app updates'
    sudo defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdate -bool false

    # Disable 'Install OSX updates'
    sudo defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdateRestartRequired -bool false

    sudo defaults write /Library/Preferences/com.apple.commerce.plist ConfigDataInstall -bool false

    # Disable 'Install system data files and security updates'
    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist ConfigDataInstall -bool false
    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist CriticalUpdateInstall -bool false
}


# -----------------------------------------------------------------------------
# Date & Time
# -----------------------------------------------------------------------------
set_datetime_preferences() {
    echo 'Setting "Date & Time" preferences...'

    # Show the date on the clock
    defaults write com.apple.menuextra.clock DateFormat "EEE MMM d  h:mm a"
}

# -----------------------------------------------------------------------------
# Time Machine
# -----------------------------------------------------------------------------
set_time_machine_preferences() {
    echo 'Setting "Time Machine" preferences...'

    # Prevent Time Machine from prompting to use new hard drives as backup volume
    defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

    # Disable local Time Machine backups
    hash tmutil >> ${LOG} 2>&1 && sudo tmutil disablelocal

    # Menu bar: hide the Time Machine, Volume, User, and Bluetooth icons
    for domain in ~/Library/Preferences/ByHost/com.apple.systemuiserver.*; do
        defaults write "${domain}" dontAutoLoad -array \
        "/System/Library/CoreServices/Menu Extras/TimeMachine.menu" \
        "/System/Library/CoreServices/Menu Extras/Volume.menu" \
        "/System/Library/CoreServices/Menu Extras/User.menu"
    done;
    defaults write com.apple.systemuiserver menuExtras -array \
      "/System/Library/CoreServices/Menu Extras/Clock.menu"
}

# -----------------------------------------------------------------------------
# Accessibility
# -----------------------------------------------------------------------------
set_accessibility_preferences() {
    echo 'Setting "Accessibility" preferences...'

    # Disable "shake mouse pointer to locate"
    defaults write ~/Library/Preferences/.GlobalPreferences CGDisableCursorLocationMagnification -bool true
}

# -----------------------------------------------------------------------------
# Finder
# -----------------------------------------------------------------------------
set_finder_preferences() {
    echo 'Setting "Finder" preferences...'

    # Remove all .DS_Store files
    # sudo find / -name ".DS_Store" -depth -exec rm {} \;

    # Automatically open a new Finder window when a volume is mounted
    defaults write com.apple.frameworks.diskimages auto-open-ro-root -bool true
    defaults write com.apple.frameworks.diskimages auto-open-rw-root -bool true
    defaults write com.apple.finder OpenWindowForNewRemovableDisk -bool true

    # Use full POSIX path as window title
    defaults write com.apple.finder _FXShowPosixPathInTitle -bool true

    # Disable window animations and Get Info animations
    defaults write com.apple.finder DisableAllAnimations -bool true

    # Disable the warning before emptying the Trash
    defaults write com.apple.finder WarnOnEmptyTrash -bool false

    # Enable `Secure Empty Trash`
    # (Security of data on disk: http://www.youtube.com/watch?v=4SSSMi4X_mA)
    #defaults write com.apple.finder EmptyTrashSecurely -bool true

    # Search the current directory by default
    defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

    # Disable the warning when changing a file extension
    defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

    # Use list view in all Finder windows by default
    # Four-letter codes for the other view modes: `icnv`, `clmv`, `Flwv`
    defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"

    # Arrange by kind by default
    defaults write com.apple.Finder FXPreferredGroupBy -string "kind"

    # New finder window opens home directory
    defaults write com.apple.finder NewWindowTarget "PfHm"
    defaults write com.apple.finder NewWindowTargetPath "file:///${HOME}/"

    # Show icons for hard drives, servers, and removable media on the desktop
    # defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
    # defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true
    # defaults write com.apple.finder ShowMountedServersOnDesktop -bool true
    # defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true

    # Don't show recent tags
    defaults write com.apple.finder ShowRecentTags -bool false

    # Show all filename extensions
    defaults write NSGlobalDomain AppleShowAllExtensions -bool true

    # Show status bar
    defaults write com.apple.finder ShowStatusBar -bool true

    # Allow text selection in Quick Look
    defaults write com.apple.finder QLEnableTextSelection -bool true

    # Enable spring loading for directories
    defaults write NSGlobalDomain com.apple.springing.enabled -bool true

    # Remove the spring loading delay for directories
    defaults write NSGlobalDomain com.apple.springing.delay -float 0

    # Show the ~/Library folder
    chflags nohidden ~/Library

    # Enable AirDrop over Ethernet and on unsupported Macs running Lion
    defaults write com.apple.NetworkBrowser BrowseAllInterfaces -bool true
}

# -----------------------------------------------------------------------------
# Misc
# -----------------------------------------------------------------------------
set_misc_preferences() {
    echo 'Setting "Misc" preferences...'

    # Disable FTP server
    sudo -s launchctl unload -w /System/Library/LaunchDaemons/ftp.plist >> ${LOG} 2>&1

    # Stop Photos from opening automatically
    defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true

    # Disable App Nap system wide (this can cause issues with socket
    # communication with background apps. eg - apps not in focus)
    defaults write NSGlobalDomain NSAppSleepDisabled -bool true

    # Reset Launchpad
    # [ -e "~/Library/Application Support/Dock/*.db" ] && rm ~/Library/Application\ Support/Dock/*.db

    # Disable the “Are you sure you want to open this application?” dialog
    defaults write com.apple.LaunchServices LSQuarantine -bool false

    # Disable Automatic Java Updates
    sudo defaults write /Library/Preferences/com.oracle.java.Java-Updater JavaAutoUpdateEnabled -bool false
}

# -----------------------------------------------------------------------------
# UI Tweaks
# -----------------------------------------------------------------------------
set_ui_tweaks() {
    echo 'Setting "UI" tweaks...'

    # Always show scrollbars
    # defaults write NSGlobalDomain AppleShowScrollBars -string “Always”

    # Disable the crash reporter
    defaults write com.apple.CrashReporter DialogType -string “none”
}

# -----------------------------------------------------------------------------
# Set preferences
# -----------------------------------------------------------------------------
set_preferences() {
    osascript -e "tell application \"System Preferences\" to quit"

    set_general_perferences
    set_display_preferences
    set_dock_preferences
    set_mission_control_preferences
    set_language_and_region_preferences
    set_security_preferences
    set_notification_preferences
    set_energy_saver_preferences
    set_mouse_preferences
    set_bluetooth_preferences
    set_sharing_preferences
    set_app_store_preferences
    set_datetime_preferences
    set_time_machine_preferences
    set_accessibility_preferences
    set_finder_preferences
    set_misc_preferences

    set_ui_tweaks
}

# -----------------------------------------------------------------------------

set_preferences

for app in "Dashboard" "Dock" "Finder" "SystemUIServer" "iTunes" "System Preferences"; do
    killall "$app" > /dev/null 2>&1
done

echo "DONE! OS X settings updated! You may need to restart."
